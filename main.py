import pygame
import sys
import time
import random

pygame.init()
size = [288, 512]

#RESIZABLE = pygame.RESIZABLE
display_surface = pygame.display.set_mode(size)#,RESIZABLE

ding = 0

keys = pygame.key.get_pressed()

pipe_image2 = pygame.image.load(r'H:\2020\python\flappy\sprites\pipe-green.png')
pipe_image2 = pygame.transform.flip(pipe_image2, False, True)


class gamevars:
    def __init__(self):
        self.state = 0
        self.score = 0

game = gamevars()

class ShowText:
    def __init__(self, x, y, c, b, text, s):
        self.x = x
        self.y = y
        self.size = s
        self.font = pygame.font.Font('freesansbold.ttf', self.size)
        self.color = c
        self.background = b
        self.text = text
        self.txt_surface = self.font.render(self.text, True, self.color)#, self.background
        self.rect = self.txt_surface.get_rect(center=(size[0]*self.x, size[1]*self.y))
        self.active = False
        self.rect.center = (size[0] * x, size[1] * y)

    def draws(self, display_surface):
        # Blit the text.
        self.rect = self.txt_surface.get_rect(center=(size[0]*self.x, size[1]*self.y))
        self.txt_surface = self.font.render(self.text, True, self.color) 
        display_surface.blit(self.txt_surface, self.rect)

    def handle_event(self, event):
        if event.type == pygame.VIDEORESIZE:
            print(event.size)
            size = event.size
            self.rect.center = (size[0] * self.x, size[1] * self.y)

class ShowButton:
    def __init__(self, x, y, w, h, c):
        self.color = c
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        self.button = pygame.Rect(0,0, size[1] * self.width, size[1] * self.height)

    def draw(self, display_surface):
        # Blit the text.
        pygame.draw.rect(display_surface, self.color, self.button)

    def handle_event(self, event):
        if event.type == pygame.VIDEORESIZE:
            print(event.size)
            size = event.size
            self.button = pygame.Rect(0,0, size[1] * self.width, size[1] * self.height)
            half = [size[0] * self.x, size[1] * self.y]
            self.button.center = half

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = event.pos
            if self.button.collidepoint(mouse_pos):
                # prints current location of mouse
                print('button was pressed at {0}'.format(mouse_pos))
                return(1)

class ScrollImage:
    def __init__(self, imageobject, l, y, flip):
        self.io = imageobject
        self.location = l
        self.y = y
        self.rect = self.io.get_rect()
        self.flip = bool(flip)

    def draw(self):
        if self.flip == True:
            display_surface.blit(pygame.transform.flip(self.io, False, True), (self.location, self.y))
        else:
            display_surface.blit(self.io, (self.location, self.y))

        self.rect = self.io.get_rect(topleft=(self.location,self.y))
    
    def movescroll(self):
        self.location = self.location - 1.25
        if self.location < -336:
            self.location = 288
            return(True)

class ScrollPipe:
    def __init__(self, imageobject, l, y, flip):
        self.io = imageobject
        self.location = l
        self.y = y
        self.y2 = y - 420
        self.rect = self.io.get_rect()
        self.flip = bool(flip)

    def draw(self):
        if self.flip == True:
            display_surface.blit(pygame.transform.flip(self.io, False, True), (self.location, self.y))
        else:
            display_surface.blit(self.io, (self.location, self.y))
            display_surface.blit(pipe_image2, (self.location, self.y2))

        self.rect = self.io.get_rect(topleft=(self.location,self.y))
        self.rect2 = pipe_image2.get_rect(topleft=(self.location,self.y2))
    
    def movescroll(self):
        self.location = self.location - 1.25
        #if self.location < -52:
            #self.location = 288
            #return(True)

class Player:
    def __init__(self, image, i2, i3):
        self.player = image
        self.player1 = image
        self.player2 = i2
        self.player3 = i3
        self.x = size[0]*0.25
        self.y = size[1]/2
        self.rect = self.player.get_rect()
        self.grav = 0
        self.vel = 2.5
        self.isJump = False
        self.flaps = 0
        self.flapdelay = 0
        self.angle = 0
        self.min_angle = -15

    def gravity(self):
        self.y += self.grav

        if self.y > 400-23 and self.y >= 0:
            self.y = 400-23
            if game.state == 2:
                game.state = 3
                self.angle = 0

        if self.flapdelay == 15:
            self.flapdelay = 0
            if self.flaps == 0:
                self.player = self.player1
            if self.flaps == 1:
                self.player = self.player2
            if self.flaps == 2:
                self.player = self.player3
            if self.flaps == 2:
                self.flaps = 0
            else:
                self.flaps += 1
        else:
            self.flapdelay += 1

        if self.angle > self.min_angle:
            self.angle += -0.75

    def draw(self, gamestate):
        display_surface.blit(pygame.transform.rotate(self.player, self.angle), (self.x,self.y))
        self.rect = self.player.get_rect(topleft=(self.x,self.y))

def main():
    #Initialize Variables
    score = 0
    clicked = False
    fps = 60
    red = (255,0,0)
    white = (255, 255, 255) 
    green = (0, 255, 0) 
    blue = (0, 0, 128)
    none = (0,0,0)
    ding = 0

    background_image = pygame.image.load(r'H:\2020\python\flappy\sprites\background-night.png')
    #window_icon = pygame.image.load(r'H:\2020\python\games\logo32x32.png')
    base_image = pygame.image.load(r'H:\2020\python\flappy\sprites\base.png')
    pipe_image = pygame.image.load(r'H:\2020\python\flappy\sprites\pipe-green.png')
    
    gameover_image = pygame.image.load(r'H:\2020\python\flappy\sprites\gameover.png')
    gamestart_image = pygame.image.load(r'H:\2020\python\flappy\sprites\message.png')

    score_text = ShowText(0.5,0.15,white,none,'0',50)
    texts = [score_text]

    #button_play = ShowButton(0.5, 0.5, 0.2, 0.1042, red)
    buttons = []

    base1 = ScrollImage(base_image, 0, 400, False)
    base2 = ScrollImage(base_image, 336, 400, False)
    scrollsbase = [base1,base2]

    scrollspipe = [ScrollPipe(pipe_image, 336, random.randint(200, 400), False)]

    duck_img_mid = pygame.image.load(r'H:\2020\python\flappy\sprites\redbird-midflap.png')
    duck_img_up = pygame.image.load(r'H:\2020\python\flappy\sprites\redbird-upflap.png')
    duck_img_down = pygame.image.load(r'H:\2020\python\flappy\sprites\redbird-downflap.png')
    duck = Player(duck_img_mid, duck_img_up, duck_img_down)
    ducks = [duck]

    #Start Window
    #size = [1280, 720]
    clock = pygame.time.Clock()
    pygame.display.set_caption('Bouncy Duck')

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            
            for text in texts:
                text.handle_event(event)
            for button in buttons:
                toodoo = 0
                toodoo = button.handle_event(event)
                if toodoo == 1:
                    game.state = 1
                    try:
                        texts.remove(text1)
                    except:
                        print("Text was unable to remove")

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    key = event.key
                    print(key)

            #Handle window resizing
            if event.type == pygame.VIDEORESIZE:
                print(event.size)
                size = event.size
                display_surface = pygame.display.set_mode(size)#,RESIZABLE
                #textRect.center = (size[0] * 0.75, size[1] * 0.75)
                button = pygame.Rect(size[0] * 0.01, size[1] * 0.01, size[1] * 0.1042, size[1] * 0.1042)

        keys = pygame.key.get_pressed()
        #duck.handle_event(keys)
        if (game.state == 0):
            if keys[pygame.K_SPACE]:
                game.state = 1

        if (game.state == 3):
            if keys[pygame.K_SPACE]:
                
                scrollspipe = [ScrollPipe(pipe_image, 336, random.randint(200, 400), False)]
                duck.angle = 0
                duck.y = size[1]/2
                duck.min_angle = -15
                game.score = 0
                duck.rect = duck.player.get_rect(topleft=(duck.x,duck.y))
                score_text.text = str(game.score)
                for text in texts:
                    text.draws(display_surface)
                game.state = 1

        #Stuff To Handle Every LOOP
        display_surface.blit(background_image, (0, 0))
        #display_surface.blit(window_icon, pygame.mouse.get_pos())

        for image in scrollspipe:
            image.draw()
            if duck.rect.colliderect(image.rect) and game.state == 1:
                #print("collision")
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\hit.ogg')
                pygame.mixer.music.play(0)
                time.sleep(0.25)
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\die.ogg')
                pygame.mixer.music.play(0)
                game.state = 2
                duck.min_angle = -65
                print('cllieded1')

            if duck.rect.colliderect(image.rect2) and game.state == 1:
                #print("collision")
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\hit.ogg')
                pygame.mixer.music.play(0)
                time.sleep(0.25)
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\die.ogg')
                pygame.mixer.music.play(0)
                game.state = 2
                duck.min_angle = -65
                print('cllieded2')

        for image in scrollsbase:
            image.draw()
            if duck.rect.colliderect(image.rect) and game.state == 1:
                #print("collision")
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\hit.ogg')
                pygame.mixer.music.play(0)
                time.sleep(0.25)
                pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\die.ogg')
                pygame.mixer.music.play(0)
                game.state = 2
                duck.min_angle = -65
                print('cllieded3')

            if game.state == 1:
                image.movescroll()

        if scrollspipe[-1].location < 168:
            scrollspipe.append(ScrollPipe(pipe_image, 336, random.randint(200, 400), False))
            #print(len(scrollspipe))

        if scrollspipe[0].location == 22.25: #64.75
            game.score += 1
            print(score)
            ding = 8
            pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\point.ogg')
            pygame.mixer.music.play(1)
            score_text.text = str(game.score)
        #print(scrollspipe[0].location)
        
        if scrollspipe[0].location < -52:
            scrollspipe.pop(0)

        if game.state == 1:
            for image in scrollspipe:
                moved = image.movescroll()
                if moved:
                    image.y = random.randint(100,400)
                    image.y2 = image.y - 420
                    #scroll4.y = scroll3.y - 420

        if game.state == 0:
            for button in buttons:
                button.draw(display_surface)
            rect33 = gamestart_image.get_rect(center = (size[0]*0.5,size[1]*0.4))
            display_surface.blit(gamestart_image, rect33)

        if game.state != 0:
            for duck in ducks:
                duck.draw(game.state)

        if game.state == 1:
            duck.gravity()
            for text in texts:
                text.draws(display_surface)

        if game.state == 3:
            rect3 = gameover_image.get_rect(center = (size[0]*0.5,size[1]*0.25))
            display_surface.blit(gameover_image, rect3)

        keys = pygame.key.get_pressed()
        #duck.handle_event(keys)
        if (game.state > 0 and game.state < 3):
            if keys[pygame.K_SPACE] and duck.y > 0 and game.state != 2:
                if duck.angle < 15:
                    duck.angle += 5

                duck.y -= duck.vel**1.25
                duck.grav = -duck.vel**1.25
                if ding < 1:
                    pygame.mixer.music.load(r'H:\2020\python\flappy\sprites\wing.ogg')
                    pygame.mixer.music.play(0)
                else:
                    ding += -1
            else:
                if duck.grav < 1.5:
                    duck.grav = duck.grav + 1
                duck.gravity()

        pygame.display.update()
        clock.tick(fps)


if __name__ == '__main__':
    main()
    pygame.quit()
    sys.exit
